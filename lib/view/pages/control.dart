import 'package:flutter/material.dart';
import 'package:table_calendar/table_calendar.dart';
import '../widgets/drawer.dart';

class ControlPage extends StatefulWidget {
  const ControlPage({super.key});

  @override
  State<ControlPage> createState() => _ControlPageState();
}

class _ControlPageState extends State<ControlPage> {
  //configuracion calendar
  CalendarFormat _calendarFormat = CalendarFormat.week;
  DateTime _focusedDay = DateTime.now();
  DateTime? _selectedDay;

  @override
  Widget build(BuildContext context) {
    double screanHeight = MediaQuery.of(context).size.height;
    double screanWidth = MediaQuery.of(context).size.width;
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: const Text(
          'Control Mensual',
          style: TextStyle(
            fontSize: 22,
            fontWeight: FontWeight.w800,
          ),
        ),
      ),
      drawer: NavHomeDrawer(),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.fromLTRB(15, 0, 15, 0),
          child: Column(
            children: [
              _calendarWidget(),
              SizedBox(
                height: screanHeight * 0.01,
              ),
              _gastoMensual(),
              SizedBox(
                height: screanHeight * 0.01,
              ),
              _exampleContainer(),
              _exampleContainer(),
              _exampleContainer(),
              _exampleContainer(),
              _exampleContainer(),
              _exampleContainer(),
              _exampleContainer(),
              _exampleContainer(),
              _exampleContainer(),
              _exampleContainer(),
              _exampleContainer(),
            ],
          ),
        ),
      ),
    );
  }

  Widget _gastoMensual() {
    double screanWidth = MediaQuery.of(context).size.width;
    return Container(
      padding: const EdgeInsets.all(8.0),
      width: screanWidth * 0.92,
      decoration: BoxDecoration(
        color: Colors.grey.shade300,
        borderRadius: BorderRadius.circular(15),
      ),
      child: Column(
        children: const [
          Text(
            'Gasto Mensual',
            style: TextStyle(
              fontSize: 20,
              fontWeight: FontWeight.w800,
            ),
          ),
          Text(
            '80.000 COP',
            style: TextStyle(
              fontSize: 18,
              fontWeight: FontWeight.w600,
            ),
          ),
        ],
      ),
    );
  }

  Widget _calendarWidget() {
    return TableCalendar(
      firstDay: DateTime.utc(2010, 10, 16),
      lastDay: DateTime.utc(2030, 3, 14),
      focusedDay: _focusedDay,
      calendarFormat: _calendarFormat,
      selectedDayPredicate: (day) {
        // Use `selectedDayPredicate` to determine which day is currently selected.
        // If this returns true, then `day` will be marked as selected.

        // Using `isSameDay` is recommended to disregard
        // the time-part of compared DateTime objects.
        return isSameDay(_selectedDay, day);
      },
      onDaySelected: (selectedDay, focusedDay) {
        if (!isSameDay(_selectedDay, selectedDay)) {
          // Call `setState()` when updating the selected day
          setState(
            () {
              _selectedDay = selectedDay;
              _focusedDay = focusedDay;
            },
          );
        }
      },
      //Cambia el formato del calendar
      onFormatChanged: (format) {
        if (_calendarFormat != format) {
          // Call `setState()` when updating calendar format
          setState(() {
            _calendarFormat = format;
          });
        }
      },

      //Mantiene Resaltado el dia actual.
      onPageChanged: (focusedDay) {
        // No need to call `setState()` here
        _focusedDay = focusedDay;
      },
    );
  }

  Widget _exampleContainer() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0, 4, 0, 4),
      child: Container(
        child: ListTile(
          title: const Text(
            'Compra Tachos para silla Sala',
            style: TextStyle(
              fontSize: 20,
              fontWeight: FontWeight.w800,
              color: Colors.black,
            ),
            textAlign: TextAlign.start,
          ),
          subtitle: const Text(
            '10.000 COP',
            style: TextStyle(
              fontSize: 16,
              fontWeight: FontWeight.w800,
              color: Colors.black87,
            ),
            textAlign: TextAlign.start,
          ),
          trailing: const Icon(
            Icons.more_vert_rounded,
            size: 30,
            color: Colors.black87,
          ),
          tileColor: Colors.grey.shade300,
          dense: false,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(20),
          ),
        ),
      ),
    );
  }
}
