import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

class LoginPage extends StatefulWidget {
  final VoidCallback showRegisterPage;
  const LoginPage({Key? key, required this.showRegisterPage}) : super(key: key);

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  // URL imagenes
  final _logoUrl = "assets/images/logo.png";

  // Text Controllers
  final _emailController = TextEditingController();
  final _passwordController = TextEditingController();

  Future signIn() async {
    await FirebaseAuth.instance.signInWithEmailAndPassword(
      email: _emailController.text.trim(),
      password: _passwordController.text.trim(),
    );
  }

  // Dispose
  // Se llama cuando este objeto y su estado se eliminan
  // del árbol de forma permanente y nunca se volverán a construir.
  @override
  void dispose() {
    _emailController.dispose();
    _passwordController.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            children: [
              _logo(),
              const Text(
                'Inicio de Sesion',
                style: TextStyle(
                  fontSize: 28,
                  fontWeight: FontWeight.w800,
                ),
              ),
              _formulario(),
              const SizedBox(
                height: 16,
              ),
              GestureDetector(
                onTap: widget.showRegisterPage,
                child: const Text(
                  'Registrate Aqui',
                  style: TextStyle(
                    fontSize: 18,
                    fontWeight: FontWeight.w800,
                    color: Colors.blue,
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  // Formulario
  Widget _formulario() {
    return Form(
      child: Padding(
        padding: const EdgeInsets.fromLTRB(15, 15, 15, 8),
        child: Column(
          children: [
            const Text(
              'Correo.',
              style: TextStyle(
                fontSize: 22,
                fontWeight: FontWeight.w800,
              ),
            ),
            const SizedBox(
              height: 16,
            ),
            _textMail(),
            const SizedBox(
              height: 16,
            ),
            const Text(
              'Contraseña.',
              style: TextStyle(
                fontSize: 22,
                fontWeight: FontWeight.w800,
              ),
            ),
            const SizedBox(
              height: 16,
            ),
            _textPassword(),
            const SizedBox(
              height: 26,
            ),
            ElevatedButton(
              onPressed: signIn,
              child: const Text(
                'Iniciar Sesion',
                style: TextStyle(
                  fontSize: 22,
                  fontWeight: FontWeight.w800,
                ),
              ),
            ),
            //_registro(),
          ],
        ),
      ),
    );
  }

  // Logo
  Widget _logo() {
    return Column(
      children: [
        const SizedBox(
          height: 50,
        ),
        Image.asset(_logoUrl),
        const SizedBox(
          height: 8,
        ),
      ],
    );
  }

  // Correo
  Widget _textMail() {
    return TextFormField(
      //Controlador
      controller: _emailController,
      //Config TextField
      textAlign: TextAlign.center,
      keyboardType: TextInputType.emailAddress,
      decoration: InputDecoration(
        hintText: 'Por favor escriba su correo aqui.',
        prefixIcon: const Icon(Icons.email),
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(15.0),
        ),
      ),
      validator: (value) {
        if (value == null || value.isEmpty) {
          return "El correo es un campo obligatorio";
        }
        return null;
      },
    );
  }

  // Password
  Widget _textPassword() {
    return TextFormField(
      //controller
      controller: _passwordController,
      //config TextField
      obscureText: true,
      textAlign: TextAlign.center,
      keyboardType: TextInputType.text,
      decoration: InputDecoration(
        hintText: 'Por favor escriba su contraseña aqui.',
        prefixIcon: const Icon(Icons.lock),
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(15.0),
        ),
      ),
      validator: (value) {
        if (value == null || value.isEmpty) {
          return "La contraseña es un campo obligatorio";
        }
        return null;
      },
    );
  }
}
