import 'package:flutter/material.dart';

class AddEntrancePage extends StatelessWidget {
  const AddEntrancePage({super.key});

  @override
  Widget build(BuildContext context) {
    double screanHeight = MediaQuery.of(context).size.height;
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: const Text(
          "Nuevo Gasto",
          style: TextStyle(
            fontSize: 22,
            fontWeight: FontWeight.w800,
          ),
        ),
        leading: GestureDetector(
          onTap: () {
            Navigator.pop(context);
          },
          child: const Icon(
            Icons.arrow_back_rounded,
          ),
        ),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.fromLTRB(15, 0, 15, 0),
          child: Column(
            children: [
              SizedBox(height: screanHeight * 0.25),
              _formConcep(),
            ],
          ),
        ),
      ),
    );
  }

  // Formato
  Widget _formConcep() {
    return Column(
      children: [
        const Text(
          'Concepto del Gasto',
          style: TextStyle(
            fontSize: 20,
            fontWeight: FontWeight.w800,
          ),
        ),
        const SizedBox(
          height: 10,
        ),
        _textConcetoGasto(),
        const SizedBox(
          height: 16,
        ),
        const Text(
          'Concepto del Gasto',
          style: TextStyle(
            fontSize: 20,
            fontWeight: FontWeight.w800,
          ),
        ),
        const SizedBox(
          height: 10,
        ),
        _textValorGasto(),
        const SizedBox(
          height: 18,
        ),
        ElevatedButton(
          onPressed: () {
            /*Logica del boton*/ // CAMBIAR AQUI
          },
          child: const Text(
            'Guardar',
            style: TextStyle(
              fontSize: 22,
              fontWeight: FontWeight.w800,
            ),
          ),
        ),
      ],
    );
  }

  // Concepto del gasto
  Widget _textConcetoGasto() {
    return TextFormField(
      textAlign: TextAlign.center,
      keyboardType: TextInputType.text,
      decoration: InputDecoration(
        hintText: 'Escriba el concepto del Gasto Aqui',
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(15.0),
        ),
      ),
      validator: (value) {
        if (value == null || value.isEmpty) {
          return "Debes de identificar tu Gasto";
        }
        return null;
      },
    );
  }

  // Valor del gasto
  Widget _textValorGasto() {
    return TextFormField(
      textAlign: TextAlign.center,
      keyboardType: TextInputType.number,
      decoration: InputDecoration(
        hintText: 'Escriba el valor del Gasto Aqui',
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(15.0),
        ),
      ),
      validator: (value) {
        if (value == null || value.isEmpty) {
          return "Debes Colocar el Valor de tu Gasto";
        }
        return null;
      },
    );
  }

  //Boton Guardar

}
