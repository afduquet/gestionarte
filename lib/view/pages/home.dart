import 'package:flutter/material.dart';
import 'package:gestion_arte/view/pages/add_new_entrance.dart';
import 'package:gestion_arte/view/widgets/drawer.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:intl/intl.dart';

class HomePage extends StatelessWidget {
  const HomePage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: const Text(
          'Gestion Diaria',
          style: TextStyle(
            fontSize: 22,
            fontWeight: FontWeight.w800,
          ),
        ),
        // leading: GestureDetector(
        //   onTap: () {
        //     /* listener */
        //   },
        //   child: const Icon(
        //     Icons.menu_rounded,
        //     color: Colors.white,
        //     size: 30,
        //   ),
        // ),
      ),
      drawer: NavHomeDrawer(),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.fromLTRB(15, 10, 15, 10),
          child: Column(
            children: [
              _showDay(),
              _exampleContainer(),
              _exampleContainer(),
              _exampleContainer()
            ],
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => AddEntrancePage(),
              ));
        },
        child: const Icon(
          Icons.add_rounded,
          color: Colors.white,
          size: 45,
        ),
      ),
    );
  }

  Widget _showDay() {
    return Container(
      height: 60,
      decoration: BoxDecoration(
        color: Colors.grey.shade200,
        borderRadius: BorderRadius.circular(15),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          _currentDate(),
        ],
      ),
    );
  }

  //Ejemplo Container

  Widget _exampleContainer() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0, 4, 0, 4),
      child: Container(
        child: ListTile(
          title: const Text(
            'Compra Tachos para silla Sala',
            style: TextStyle(
              fontSize: 20,
              fontWeight: FontWeight.w800,
              color: Colors.black,
            ),
            textAlign: TextAlign.start,
          ),
          subtitle: const Text(
            '10.000 COP',
            style: TextStyle(
              fontSize: 16,
              fontWeight: FontWeight.w800,
              color: Colors.black87,
            ),
            textAlign: TextAlign.start,
          ),
          trailing: const Icon(
            Icons.more_vert_rounded,
            size: 30,
            color: Colors.black87,
          ),
          tileColor: Colors.grey.shade300,
          dense: false,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(20),
          ),
        ),
      ),
    );
  }

  Widget _currentDate() {
    var now = DateTime.now();
    var formatter = DateFormat("EEEE, d 'de' MMMM 'de' y", "es");

    var date = formatter.format(now);
    return Text(
      date,
      style: const TextStyle(
        fontWeight: FontWeight.bold,
        fontSize: 22,
      ),
    );
  }
}
