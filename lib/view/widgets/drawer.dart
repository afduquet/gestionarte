import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:gestion_arte/view/pages/control.dart';
import '/view/pages/home.dart';

class NavHomeDrawer extends StatefulWidget {
  const NavHomeDrawer({super.key});

  @override
  State<NavHomeDrawer> createState() => _NavHomeDrawerState();
}

class _NavHomeDrawerState extends State<NavHomeDrawer> {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: <Widget>[
          DrawerHeader(
            decoration: const BoxDecoration(color: Colors.blue),
            child: _headerContent(),
          ),
          ListTile(
            leading: const Icon(
              Icons.home_rounded,
              size: 30,
            ),
            title: const Text(
              'Inicio',
              style: TextStyle(
                fontSize: 18,
                fontWeight: FontWeight.bold,
              ),
            ),
            onTap: () {
              Navigator.pushReplacement(
                  context,
                  MaterialPageRoute(
                    builder: (context) => HomePage(),
                  ));
            },
          ),
          ListTile(
            leading: const Icon(
              Icons.calendar_month_rounded,
              size: 30,
            ),
            title: const Text(
              'control',
              style: TextStyle(
                fontSize: 18,
                fontWeight: FontWeight.bold,
              ),
            ),
            onTap: () {
              Navigator.pushReplacement(
                  context,
                  MaterialPageRoute(
                    builder: (context) => ControlPage(),
                  ));
            },
          ),
          ListTile(
            leading: const Icon(
              Icons.close,
              size: 30,
            ),
            title: const Text(
              'Cerrar Sesion',
              style: TextStyle(
                fontSize: 18,
                fontWeight: FontWeight.bold,
              ),
            ),
            onTap: () {
              FirebaseAuth.instance.signOut();
            },
          ),
        ],
      ),
    );
  }

  Widget _headerContent() {
    return Column(
      children: [
        const SizedBox(
          height: 10,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: const [
            CircleAvatar(
              child: Icon(
                Icons.supervisor_account,
                size: 40,
              ),
              maxRadius: 30,
            ),
          ],
        ),
        Column(
          children: const [
            SizedBox(
              height: 16,
            ),
            Text('Nombre'),
            SizedBox(
              height: 10,
            ),
            Text('emai@email.com')
          ],
        )
      ],
    );
  }
}
